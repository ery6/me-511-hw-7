#!/bin/bash
#SBATCH --job-name=ME511
#SBATCH --nodes 1
#SBATCH --ntasks-per-node=20
#SBATCH --cpus-per-task=2
#SBATCH -q long
#SBATCH --time=5:00:00

# initialization                                                                                                                                  
module load FHIaims

cd $SLURM_SUBMIT_DIR/

# Run FHI-aims and writes an output file called "aims.out".
srun -n $SLURM_NTASKS aims.231208.scalapack.mpi.x > aims.out 2>&1

# Note that you could continue with commands to do all sorts of things
# before your job ends. This is a real "shell script" and you could
# modify it at will to carry out other tasks. The present version just
# runs FHI-aims and is done.
